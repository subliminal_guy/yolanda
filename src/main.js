import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import axios from 'axios'

import routes from './router';

import store from './store';

Vue.config.productionTip = false
axios.defaults.baseURL = "https://safe-hamlet-27016.herokuapp.com/"

Vue.use(VueRouter, axios)
const router = new VueRouter({routes});

new Vue({
  router,
  store,
  beforeCreate() {this.$store.commit('initialiseStore')},
  render: h => h(App),
}).$mount('#app')
