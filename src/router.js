import Vue from 'vue';
import Router from 'vue-router'
import LandingPage from './pages/LandingPage';
import Organisations from './pages/Organisations';
import Volunteers from './pages/Volunteers';
import _voloId from './pages/_voloId'
import _orgId from './pages/_orgId'


Vue.use(Router);

const routes =  [
    { path: '/', component: LandingPage},
    { path: '/organisations', component: Organisations},
    { path: '/organisations/:id', component: _orgId},
    { path: '/volunteers', component: Volunteers},
    { path: '/volunteers/:id', component: _voloId}
  ]


export default routes;