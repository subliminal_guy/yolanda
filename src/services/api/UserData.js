import axios from 'axios'

export default {
    getOrganisations () {
        return axios.get("/organisations")
        .then(response => {
            return response.data
    })
    },
    
    getVolunteers () {
        return axios.get("/volunteers")
        .then(response => {
            return response.data
        })
    },

}